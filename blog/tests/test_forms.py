from blog import forms

class Testpostform:
  def test_form(self):
      form = forms.postform(data={'title':'', 'body':'', 'author':''})
      assert form.is_valid() is False, 'Form should not be empty'

      form = forms.postform(data={'title':'my title', 'body':'my bo123456789789561230dy', 'author':'my author'})
      assert form.is_valid() is False, 'should be invalid if input is too short'
      assert 'body' in form.errors, 'should have body field error'

      long_body = "© 2019 LinkedIn Ireland Unlimited Company, Wilton Plaza, Wilton Place, Dublin 2. LinkedIn is a registered business name of LinkedIn Ireland Unlimited Company. LinkedIn and the LinkedIn logo are registered trademarks of LinkedIn."

      form = forms.postform(data={'title':'my title', 'body':long_body, 'author':'my author'})
      assert form.is_valid() is True, 'form valid if response is long enough'
