from django.test import RequestFactory
from blog import views
import pytest
from mixer.backend.django import mixer
from django.contrib.auth.models import AnonymousUser
pytestmark = pytest.mark.django_db



class Testhome_view:
    def test_home_view(self):
        request = RequestFactory().get('/')
        resp = views.home_view(request)
        assert resp.status_code == 200, 'Should be callable by anyone'


class Testnewpost_view:
    def test_not_authenticated(self):
        request = RequestFactory().get('/')
        request.user=AnonymousUser()
        resp = views.newpost_view(request)
        assert 'login' in resp.url, 'Should redirect to login page'


    def test_authenticated(self):
        request = RequestFactory().get('/')
        user = mixer.blend('auth.User', is_superuser=False)
        request.user = user
        resp = views.newpost_view(request)
        assert resp.status_code==200, 'Should be available to authenticated User'
