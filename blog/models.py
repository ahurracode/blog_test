from django.db import models
from django.core.validators import MinLengthValidator




class Post(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField(max_length=500, validators=[MinLengthValidator(30)])
    author = models.CharField(max_length=50)
    timestamp = models.DateTimeField(auto_now_add = True)



    def __str__(self):
        return self.title
