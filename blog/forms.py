from django import forms
from blog import models


class postform(forms.ModelForm):
    class Meta:
        model = models.Post
        fields = ['title', 'body', 'author']
