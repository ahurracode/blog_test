from django.shortcuts import render, redirect
from .models import Post
from blog import forms
from django.contrib.auth.decorators import login_required

def home_view(request):
    posts = Post.objects.all().order_by('timestamp')
    return render(request, 'blog/posts.html', {'posts':posts})

@login_required()
def newpost_view(request):
    if request.method == 'POST':
        form = forms.postform(request.POST)
        if form.is_valid():
            #save t # DEBUG:
            form.save()
            return redirect('blog:home')

    else:
        form = forms.postform()
    return render(request, 'blog/newpost.html', {'form':form})
