from django.urls import path, include
from blog import views

app_name = 'blog'
urlpatterns = [
    path('', views.home_view, name='home'),
    path('newpost/', views.newpost_view, name='newpost')
]
